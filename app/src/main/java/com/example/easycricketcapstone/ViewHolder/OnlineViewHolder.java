package com.example.easycricketcapstone.ViewHolder;

import android.net.Uri;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.easycricketcapstone.R;

public class OnlineViewHolder extends RecyclerView.ViewHolder
{

    public TextView playerName;
    public TextView topScore;
    public Button chalangeButton;

    public OnlineViewHolder(@NonNull View itemView)
    {
        super(itemView);
        playerName = (TextView)itemView.findViewById(R.id.name);
    }
}
