package com.example.easycricketcapstone;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;


public class TossActivity extends Activity
{

    private ImageView imageview1;
    private ImageView imageview2;
    private Button btn1;
    private Button btn2;
    private Button btn3;
    private Button btn4;
    private Button btn5;
    private Button btn6;
    String emailId;

    private TextView textview3;
    private TextView textview4;

    private double x = 0;
    private double y = 0;
    private double z = 0;
    private double num = 0;


    private Timer timer = new Timer();
    private Intent toss2 = new Intent();
    private TimerTask checkTime;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.toss);
        init();
        initLogic();
        emailId = getIntent().getStringExtra("emailId");
    }

    private void  init()
    {
        imageview1 = (ImageView) findViewById(R.id.imageview1);
        imageview2 = (ImageView) findViewById(R.id.imageview2);
        btn1 = (Button) findViewById(R.id.button1);
        btn2 = (Button) findViewById(R.id.button2);
        btn3 = (Button) findViewById(R.id.button3);
        btn4 = (Button) findViewById(R.id.button4);
        btn5 = (Button) findViewById(R.id.button5);
        btn6 = (Button) findViewById(R.id.button6);


        btn1.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View _v)
            {
                x = 1;
                y = getRandom();
                DisplayImage();
                imageview2.setImageResource(R.drawable.hands1f);
                StartNewActivity();
            }
        });

        btn2.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View _v)
            {
                x = 2;
                y = getRandom();
                DisplayImage();
                imageview2.setImageResource(R.drawable.hands2f);
                StartNewActivity();
            }
        });

        btn3.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View _v)
            {
                x = 3;
                y = getRandom();
                imageview2.setImageResource(R.drawable.hands3f);
                DisplayImage();
                StartNewActivity();
            }
        });

        btn4.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View _v)
            {
                x = 4;
                y = getRandom();
                imageview2.setImageResource(R.drawable.hands4f);
                DisplayImage();
                StartNewActivity();
            }
        });

        btn5.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View _v)
            {
                x = 5;
                y = getRandom();
                imageview2.setImageResource(R.drawable.hands5f);
                DisplayImage();
                StartNewActivity();
            }
        });

        btn6.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View _v)
            {
                x = 6;
                y = getRandom();
                imageview2.setImageResource(R.drawable.hand6f);
                DisplayImage();
                StartNewActivity();
            }
        });

    }

    private int getRandom()
    {
        Random random = new Random();
        return random.nextInt(6) + 1;
    }

    private void  initLogic()
    {
        num = 1;
    }


    private void StartNewActivity ()
    {
        if (num == 1)
        {
            num = num + 1;
            z = Double.parseDouble(getIntent().getStringExtra("r"));
            if (((x + y) % 2) == z)
            {
                checkTime = new TimerTask()
                {
                    @Override
                    public void run()
                    {
                        runOnUiThread(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                Toast.makeText(getApplicationContext(), "You won the toss", Toast.LENGTH_SHORT).show();
                                toss2.setClass(getApplicationContext(), Toss2Activity.class);
                                startActivity(toss2);
                                finish();
                            }
                        });
                    }
                };
                timer.schedule(checkTime, (int)(2000));
            }
            else
            {
                checkTime = new TimerTask()
                {
                    @Override
                    public void run()
                    {
                        runOnUiThread(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                Toast.makeText(getApplicationContext(), "System won the tose", Toast.LENGTH_SHORT).show();
                                toss2.setClass(getApplicationContext(), GameActivity.class);
                                toss2.putExtra("role", "2");
                                startActivity(toss2);
                                finish();
                            }
                        });
                    }
                };
                timer.schedule(checkTime, (int)(2000));
            }
        }
    }
    private void DisplayImage ()
    {

        if (y == 1)
        {
            imageview1.setImageResource(R.drawable.hands1f);
        }
        if (y == 2)
        {
            imageview1.setImageResource(R.drawable.hands2f);
        }
        if (y == 3)
        {
            imageview1.setImageResource(R.drawable.hands3f);
        }
        if (y == 4)
        {
            imageview1.setImageResource(R.drawable.hands4f);
        }
        if (y == 5)
        {
            imageview1.setImageResource(R.drawable.hands5f);
        }
        if (y == 6)
        {
            imageview1.setImageResource(R.drawable.hand6f);
        }
    }

}
