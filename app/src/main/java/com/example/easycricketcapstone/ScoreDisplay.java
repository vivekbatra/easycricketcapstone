package com.example.easycricketcapstone;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ScoreDisplay extends AppCompatActivity
{

    private TextView txt1;
    private TextView txt2;
    private TextView txt3;

    private Button btn1;
    private double a = 0;
    private double b = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.score_display);
        init();
        initLogic();
    }

    private void  init()
    {
        txt1 = (TextView) findViewById(R.id.textview1);
        txt2 = (TextView) findViewById(R.id.textview3);
        txt3 = (TextView) findViewById(R.id.textview5);
        btn1 = (Button) findViewById(R.id.button1);

        btn1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View _v)
            {
                Intent intent = new Intent(getApplicationContext(), Menu.class);
                startActivity(intent);
            }
        });
    }

    private void  initLogic()
    {
        a = Double.parseDouble(getIntent().getStringExtra("x"));
        b = Double.parseDouble(getIntent().getStringExtra("y"));
        txt2.setText(String.valueOf((long)(a)));
        txt3.setText(String.valueOf((long)(b)));
        if (a > b) {
            txt1.setText("YOU WIN");
        }
        else {
            txt1.setText("YOU LOST");
        }
    }


}