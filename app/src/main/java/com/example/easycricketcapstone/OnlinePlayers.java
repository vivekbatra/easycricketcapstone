package com.example.easycricketcapstone;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.example.easycricketcapstone.Model.Game;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import java.util.HashMap;
import java.util.Map;
import butterknife.BindView;
import butterknife.ButterKnife;

public class OnlinePlayers extends AppCompatActivity
{
    public static String currentid = "";
    RecyclerView recy_Players;
    FirebaseRecyclerAdapter<Game, CurrentGameViewHolder> adapter;
    private DatabaseReference currentDatabase;
    private FirebaseUser currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.online);
        ButterKnife.bind(this);
        currentDatabase = FirebaseDatabase.getInstance().getReference();
        currentUser = FirebaseAuth.getInstance().getCurrentUser();
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        attachRecyclerViewAdapter();
        recy_Players.setAdapter(adapter);
        adapter.startListening();
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        adapter.stopListening();
    }

    private void attachRecyclerViewAdapter()
    {
        Query query = currentDatabase.child("games");
        FirebaseRecyclerOptions<Game> options = new FirebaseRecyclerOptions.Builder<Game>()
                .setQuery(query, Game.class)
                .build();
        adapter =
                new FirebaseRecyclerAdapter<Game, CurrentGameViewHolder>(options)
                {
                    @Override
                    public CurrentGameViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
                    {
                        return new CurrentGameViewHolder(LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.item_game, parent, false)
                        );
                    }

                    @Override
                    protected void onBindViewHolder(CurrentGameViewHolder holder, int position, Game model)
                    {
                        holder.bind(model);
                    }
                };
        recy_Players = (RecyclerView) findViewById(R.id.recycler_games);
        recy_Players.setHasFixedSize(true);
        recy_Players.setLayoutManager(new LinearLayoutManager(this));
    }


    private void moveToActivity()
    {
        Map<String, Object> childUpdates = new HashMap<>(1);
        childUpdates.put("games/" + currentid + "/players/" + currentUser.getUid(), currentUser.getDisplayName());
        currentDatabase.updateChildren(childUpdates);
        currentDatabase.addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot)
            {
                int count = (int)dataSnapshot.child("games").child(currentid).child("players").getChildrenCount();
                if (count == 2)
                {
                    Intent intent = new Intent(OnlinePlayers.this, OnlineToss.class);
                    startActivity(intent);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError)
            {

            }
        });
    }

    class CurrentGameViewHolder extends RecyclerView.ViewHolder
    {
        @BindView(R.id.text_game_name)
        TextView textGameName;
        Game currentGame;

        public CurrentGameViewHolder(View itemView)
        {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    final TextView textViewName = (TextView) view.findViewById(R.id.text_game_name);
                    final String gameName = textViewName.getText().toString();

                    currentDatabase.child("games").orderByChild("name").equalTo(gameName).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot)
                        {

                            for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren())
                            {
                                currentid = dataSnapshot1.getKey();
                            }
                        }
                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                        }
                    });

                        moveToActivity();
                }
            });
        }
        public void bind(Game currentGame)
        {
            this.currentGame = currentGame;
            textGameName.setText(currentGame.getName());
        }
    }
}
