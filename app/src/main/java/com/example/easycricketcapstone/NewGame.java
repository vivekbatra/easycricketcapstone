package com.example.easycricketcapstone;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import com.example.easycricketcapstone.Model.Game;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import java.util.SortedMap;
import java.util.TreeMap;


public class NewGame extends AppCompatActivity
{
    EditText editNewGame;
    Button buttonSubmit;
    ProgressBar progressSubmit;

    private DatabaseReference mDatabase;
    private FirebaseUser mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_game2);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mUser = FirebaseAuth.getInstance().getCurrentUser();
        editNewGame = (EditText) findViewById(R.id.edit_new_name);
         buttonSubmit = (Button) findViewById(R.id.button_new_game_submit);
        progressSubmit = (ProgressBar) findViewById(R.id.progress_new_game_submit);
    }

    public void submit(View view)
    {
        {
            progressSubmit.setVisibility(View.VISIBLE);
            buttonSubmit.setEnabled(false);

            String name = editNewGame.getText().toString();
            String count = "1";


            SortedMap<String, String> players = new TreeMap<>();
            players.put(mUser.getUid(), mUser.getDisplayName());
            Game game = new Game(name, players, count);

            final String key = mDatabase.child("games").push().getKey();

            mDatabase.child("games").child(key).setValue(game).addOnCompleteListener(new OnCompleteListener<Void>()
            {
                @Override
                public void onComplete(@NonNull Task<Void> task)
                {
                    if (task.isComplete())
                    {
                        progressSubmit.setVisibility(View.GONE);
                        buttonSubmit.setEnabled(true);
                    }
                    if (task.isSuccessful())
                    {
                        mDatabase.child("games").child(key).child("batting").setValue(mUser.getUid());
                        Intent intent = new Intent(NewGame.this, OnlinePlayers.class);
                        startActivity(intent);
                    }
                }
            });
        }


    }
}
