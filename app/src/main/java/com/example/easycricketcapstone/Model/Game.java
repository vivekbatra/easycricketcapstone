package com.example.easycricketcapstone.Model;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.List;
import java.util.Map;


@IgnoreExtraProperties
public class Game {
    private String name;
    private String state;
    private Map<String, String> players;
    private List<String> winners;
    private String password;
    private String id;
    private String count;




    public Game() {
    }

    public Game(String name, String password, Map<String, String> players) {
        this.name = name;
        this.password = password;
        this.state = "CREATED";
        this.players = players;

    }

    public Game(String name, String state, Map<String, String> players, List<String> winners, String password) {
        this.name = name;
        this.state = state;
        this.players = players;
        this.winners = winners;
        this.password = password;
    }
    public Game(String name, Map<String, String> players, String count) {
        this.name = name;
        this.state = "CREATED";
        this.players = players;
        this.count = count;


    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Map<String, String> getPlayers() {
        return players;
    }

    public void setPlayers(Map<String, String> players) {
        this.players = players;
    }

    public List<String> getWinners() {
        return winners;
    }

    public void setWinners(List<String> winners) {
        this.winners = winners;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public boolean hasPassword() {
        return !("".equals(this.password));
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
