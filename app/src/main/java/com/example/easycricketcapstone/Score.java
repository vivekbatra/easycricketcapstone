package com.example.easycricketcapstone;

public class Score
{
    String runs;

    public Score(String runs) {
        this.runs = runs;
    }

    public String getRuns() {
        return runs;
    }

    public void setRuns(String runs) {
        this.runs = runs;
    }
}
