package com.example.easycricketcapstone.adapters;

import android.util.Log;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.easycricketcapstone.Model.Player;
import com.example.easycricketcapstone.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;



public class PlayersAdapter extends RecyclerView.Adapter<PlayersAdapter.PlayerViewHolder>
{

    public List<Player> players;

    public PlayersAdapter(List<Player> players) {
        this.players = players;
    }

    @Override
    public PlayerViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        return new PlayerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_player, parent, false));

    }

    @Override
    public void onBindViewHolder(PlayerViewHolder holder, int position)
    {
        holder.bind(players.get(position));
    }

    @Override
    public int getItemCount() {
        return players.size();
    }

    public void addAll(List<Player> playerList)
    {
        players.clear();
        players.addAll(playerList);
        notifyDataSetChanged();
    }

    class PlayerViewHolder extends RecyclerView.ViewHolder
    {
        @BindView(R.id.text_player)TextView textGameName;

        public PlayerViewHolder(View itemView)
        {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
        @Nullable
        public void bind(Player player)
        {
            textGameName.setText(player.getName());
        }
    }
}
