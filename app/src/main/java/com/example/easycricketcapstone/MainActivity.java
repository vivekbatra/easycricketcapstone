package com.example.easycricketcapstone;
import android.app.*;
import android.os.*;
import android.content.*;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import androidx.annotation.NonNull;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Arrays;
import java.util.List;

import butterknife.ButterKnife;


public class MainActivity extends Activity
{

    String TAG = "MainActivity";
    private static final int RC_SIGN_IN = 123;
    private FirebaseUser currentUser;
    private DatabaseReference databaseReference;



    List<AuthUI.IdpConfig> providers = Arrays.asList(
            new AuthUI.IdpConfig.EmailBuilder().build(),
            new AuthUI.IdpConfig.GoogleBuilder().build());


    Button tossBtn;
    Button onlineBtn;
    Button inviteBtn;
    Button logOutBtn, newGameBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tossBtn = (Button) findViewById(R.id.startButton);
        onlineBtn = (Button) findViewById(R.id.onlineButton);
        logOutBtn = (Button) findViewById(R.id.logoutButton);
        newGameBtn = (Button) findViewById(R.id.newGameButton);


        ButterKnife.bind(this);
        FirebaseAuth auth = FirebaseAuth.getInstance();
        if (auth.getCurrentUser() != null)
        {
            Log.i(TAG, "Current User: " + auth.getCurrentUser().getUid());
            currentUser = FirebaseAuth.getInstance().getCurrentUser();
            databaseReference = FirebaseDatabase.getInstance().getReference();

            databaseReference.child("player").addListenerForSingleValueEvent(new ValueEventListener()
            {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot)
                {
                    if (dataSnapshot.child(currentUser.getUid()).exists())
                    {
                        return;
                    }
                    else
                    {
                        databaseReference.child("player").child(currentUser.getUid()).child("name").setValue(currentUser.getDisplayName());
                        databaseReference.child("player").child(currentUser.getUid()).child("email").setValue(currentUser.getEmail());
                        databaseReference.child("player").child(currentUser.getUid()).child("highestScore").setValue("0");
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        } else
        {
            startActivityForResult(
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .setAvailableProviders(providers)
                            .build(),
                    RC_SIGN_IN
            );
        }

  tossBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(getApplicationContext(), Menu.class);
                startActivity(intent);
            }
        });

        onlineBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(getApplicationContext(), OnlinePlayers.class);
                startActivity(intent);
            }
        });

        newGameBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(MainActivity.this, NewGame.class);
                startActivity(intent);
            }
        });

        logOutBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(getApplicationContext(), Register.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onActivityResult ( int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);

            if (resultCode == RESULT_OK)
            {
                Log.i(TAG, "Signed In");
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);

            } else {
                if (response == null) {
                    Log.i(TAG, "User cancelled");
                }
            }
        }
    }
}
