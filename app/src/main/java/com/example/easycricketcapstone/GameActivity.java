package com.example.easycricketcapstone;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Random;


public class GameActivity extends Activity
{
     ImageView imgView1;

    private FirebaseUser currentUser;
    private DatabaseReference databaseReference;
    String highgestScore;

    Button btnNum1;
    Button btnNum2;
    Button btnNum3;
    Button btnNum4;
    Button btnNum5;
    Button btnNum6;
    ImageView imgView2;
    TextView systemScore, playerScore, systemTotalScore, playerTotalScore;

    int round = 0;
    int a = 0;
    double role = 0;
    int random = 0;
    int x = 0;
    int y = 0;

     Intent score = new Intent();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        setContentView(R.layout.game);
        init();
        initLogic();
    }

    public void  init()
    {
        imgView1 = (ImageView) findViewById(R.id.imageview1);
        btnNum1 = (Button) findViewById(R.id.button1);
        btnNum2 = (Button) findViewById(R.id.button2);
        btnNum3 = (Button) findViewById(R.id.button3);
        btnNum4 = (Button) findViewById(R.id.button4);
        btnNum5 = (Button) findViewById(R.id.button5);
        btnNum6 = (Button) findViewById(R.id.button6);
        imgView2 = (ImageView) findViewById(R.id.imageview2);
        systemScore = (TextView) findViewById(R.id.textview1);
        playerScore = (TextView) findViewById(R.id.textview2);
        systemTotalScore = (TextView) findViewById(R.id.textview3);
        playerTotalScore = (TextView) findViewById(R.id.textview4);

        btnNum1.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                a = 1;
                imgView2.setImageResource(R.drawable.hands1f);
                CalculateValue();
            }
        });
        btnNum2.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                a = 2;
                imgView2.setImageResource(R.drawable.hands2f);
                CalculateValue();
            }
        });
        btnNum3.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                a = 3;
                imgView2.setImageResource(R.drawable.hands3f);
                CalculateValue();
            }
        });
        btnNum4.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                a = 4;
                imgView2.setImageResource(R.drawable.hands4f);
                CalculateValue();
            }
        });
        btnNum5.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                a = 5;
                imgView2.setImageResource(R.drawable.hands5f);
                CalculateValue();
            }
        });
        btnNum6.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                a = 6;
                imgView2.setImageResource(R.drawable.hand6f);
                CalculateValue();
            }
        });

    }

    public void  initLogic()
    {
        round = 1;
        x = 0;
        y = 0;
        role = Double.parseDouble(getIntent().getStringExtra("role"));
    }

    public void CalculateValue ()
    {
        role = Double.parseDouble(getIntent().getStringExtra("role"));
        random = getRandom((int)(1), (int)(6));
        systemScore.setText("System : " + random);
        playerScore.setText("Player : " + a);

        ShowImage();
        if (round == 1)
        {
            if (random == a)
            {
                if (role == 1)
                {
                    role = 2;
                    round = 2;
                    DisplayMe("Batting is over");
                }
                else
                {
                    role = 1;
                    round = 2;
                    DisplayMe("Bowling is over");
                }
            }
            else
            {
                if (role == 1)
                {
                    x = x + a;
                }
                else
                {
                    y = y + random;
                }
            }
            playerTotalScore.setText("Player Total : " + x);
            systemTotalScore.setText("System total : " + y);
        }
        else
        {
            databaseReference.child("player").child(currentUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener()
            {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot)
                {

                    highgestScore = dataSnapshot.child("highestScore").getValue(String.class);
                    if(Integer.parseInt(highgestScore) < x)
                    {
                        databaseReference.child("player").child(currentUser.getUid()).child("highestScore").setValue(Integer.toString(x));
                    }

                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            if (random == a)
            {
                if (role == 1)
                {
                    if (x > y)
                    {
                        DisplayMe("You win");
                        ScoreActivity();
                        finish();
                    }
                }
                else
                {
                    DisplayMe("You lost");
                    ScoreActivity();
                    finish();
                }
            }
            else
            {
                if (role == 1)
                {
                    y = y + random;
                    if (y > x)
                    {
                        DisplayMe("You lose");
                        ScoreActivity();
                        finish();
                    }
                }
                else
                {
                    x = x + a;
                    if (x > y)
                    {
                        DisplayMe("You win");
                        ScoreActivity();
                        finish();
                    }
                }
            }
        }
    }
    public void ScoreActivity ()
    {
        score.setClass(getApplicationContext(), ScoreDisplay.class);
        score.putExtra("x", String.valueOf((long)(x)));
        score.putExtra("y", String.valueOf((long)(y)));
        startActivity(score);
    }
    public void ShowImage ()
    {
        if (random == 1)
        {
            imgView1.setImageResource(R.drawable.hands1f);
        }
        if (random == 2)
        {
            imgView1.setImageResource(R.drawable.hands2f);
        }
        if (random == 3)
        {
            imgView1.setImageResource(R.drawable.hands3f);
        }
        if (random == 4)
        {
            imgView1.setImageResource(R.drawable.hands4f);
        }
        if (random == 5)
        {
            imgView1.setImageResource(R.drawable.hands5f);
        }
        if (random == 6)
        {
            imgView1.setImageResource(R.drawable.hand6f);
        }
    }
    public void DisplayMe(String show)
    {
        Toast.makeText(getApplicationContext(), show, Toast.LENGTH_SHORT).show();
    }

    public int getRandom(int minValue ,int maxValue)
    {
        Random random = new Random();
        return random.nextInt(maxValue - minValue + 1) + minValue;
    }
}
