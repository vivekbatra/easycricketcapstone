package com.example.easycricketcapstone;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Menu extends Activity
{
    Button button1;
    Button button2;

    Intent toss = new Intent();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu);
        initialize();
    }

    private void  initialize()
    {
        button1 = (Button) findViewById(R.id.button1);
        button2 = (Button) findViewById(R.id.button2);

        button1.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View _v)
            {
                toss.setClass(getApplicationContext(), TossActivity.class);
                toss.putExtra("r", "1");
                startActivity(toss);
            }
        });
        button2.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View _v)
            {
                toss.setClass(getApplicationContext(), TossActivity.class);
                toss.putExtra("r", "0");
                startActivity(toss);
            }
        });
    }
}
