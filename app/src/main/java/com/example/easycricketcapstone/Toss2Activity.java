package com.example.easycricketcapstone;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;



public class Toss2Activity extends Activity
{

    private Button button5;
    private Button button6;

    private double role = 0;
    private double r = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.toss2);
        initialize();
    }

    private void  initialize()
    {
        button5 = (Button) findViewById(R.id.button5);
        button6 = (Button) findViewById(R.id.button6);

        button5.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View _v)
            {
                Intent intent = new Intent(Toss2Activity.this, GameActivity.class);
                intent.putExtra("role", "1");
                startActivity(intent);
            }
        });
        button6.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View _v) {
                Intent intent = new Intent(Toss2Activity.this, GameActivity.class);
                intent.putExtra("role", "2");
                startActivity(intent);
            }
        });

    }
}
