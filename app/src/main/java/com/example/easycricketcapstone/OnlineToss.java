
package com.example.easycricketcapstone;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;


public class OnlineToss extends AppCompatActivity
{

    public static String otherUserId;

    private ImageView imageview1;
    private ImageView imageview2;
    private Button btnNum1;
    private Button btnNum2;
    private Button btnNum3;
    private Button btnNum4;
    private Button btnNum5;
    private Button btnNum6;

    TextView textview3, textview1, textview2, battinStatus;
    TextView textview4;

    private int x = 0;
    private int y = 0;
    private int z = 0;
    String gameId = "";
     double num = 0;
    int playerTotal = 0;
    int otherPlayerTotal =0;
    int xWas = 0;
    int yWas = 0;




    int playerTotalSent = 0;
    long playerTotalReceived = 0;
    long opponentTotalReceived = 0;



    private DatabaseReference currentDatabase;
    private DatabaseReference otheruserDatabase;
    private FirebaseUser currentUser;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_toss);


        imageview1 = (ImageView) findViewById(R.id.imageview1);
        imageview2 = (ImageView) findViewById(R.id.imageview2);
        btnNum1 = (Button) findViewById(R.id.button1);
        btnNum2 = (Button) findViewById(R.id.button2);
        btnNum3 = (Button) findViewById(R.id.button3);
        btnNum4 = (Button) findViewById(R.id.button4);
        btnNum5 = (Button) findViewById(R.id.button5);
        btnNum6 = (Button) findViewById(R.id.button6);
        textview1 = (TextView) findViewById(R.id.textview1);
        textview2 = (TextView) findViewById(R.id.textview2);
        battinStatus = (TextView) findViewById(R.id.battingStatus);


        textview3 = (TextView) findViewById(R.id.textview3);
        textview4 = (TextView) findViewById(R.id.textview4);

        currentDatabase = FirebaseDatabase.getInstance().getReference();
        otheruserDatabase = FirebaseDatabase.getInstance().getReference();
        currentUser = FirebaseAuth.getInstance().getCurrentUser();


        battinStatus.setText(currentUser.getDisplayName());


        btnNum1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View _v)
            {
                x = 1;
                playerTotalSent = playerTotalSent + x;

                setDataOnFirebase();
            }
        });

        btnNum2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View _v)
            {
                x = 2;
                playerTotalSent = playerTotalSent + x;

                setDataOnFirebase();
            }
        });

        btnNum3.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View _v)
            {
                x = 3;
                playerTotalSent = playerTotalSent + x;

                setDataOnFirebase();
            }
        });

        btnNum4.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View _v)
            {
                x = 4;
                playerTotalSent = playerTotalSent + x;

                setDataOnFirebase();
            }
        });

        btnNum5.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View _v)
            {
                x = 5;
                playerTotalSent = playerTotalSent + x;

                setDataOnFirebase();
            }
        });

        btnNum6.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View _v)
            {
                x = 6;
                playerTotalSent = playerTotalSent + x;
                setDataOnFirebase();
            }
        });


        init();
        initLogic();

    }


    private void  init()
    {
        if (!OnlinePlayers.currentid.isEmpty())
        {
            gameId = OnlinePlayers.currentid;
            currentDatabase.child("games").child(gameId).child("count").setValue("2");

            currentDatabase.child("games").child(gameId).child("count").addListenerForSingleValueEvent(new ValueEventListener()
            {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot)
                {
                    @NonNull
                    String a = dataSnapshot.getValue(String.class);
                    if (a.equalsIgnoreCase("2"))
                    {
                        imageview1.setVisibility(View.VISIBLE);
                        imageview2.setVisibility(View.VISIBLE);
                        btnNum1.setVisibility(View.VISIBLE);
                        btnNum2.setVisibility(View.VISIBLE);
                        btnNum3.setVisibility(View.VISIBLE);
                        btnNum4.setVisibility(View.VISIBLE);
                        btnNum5.setVisibility(View.VISIBLE);
                        btnNum6.setVisibility(View.VISIBLE);
                    }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });



//            mDatabase.child("game").child(gameId).child("batting").addListenerForSingleValueEvent(new ValueEventListener()
//            {
//                @Override
//                public void onDataChange(@NonNull DataSnapshot dataSnapshot)
//                {
//                    long whoIsBattin = dataSnapshot.getValue(Long.class);
//                    Toast.makeText(OnlineToss.this, ""+whoIsBattin, Toast.LENGTH_SHORT).show();
//
//                }
//
//                @Override
//                public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                }
//            });
        }
    }

    private void setDataOnFirebase()
    {
        String xValue = Integer.toString(x);
        currentDatabase.child("current_game").child(gameId).child(currentUser.getUid()).child("toss").setValue(xValue);
        currentDatabase.child("current_game").child(gameId).child(currentUser.getUid()).child("bool").setValue("true");
        currentDatabase.child("current_game").child(gameId).child(currentUser.getUid()).child("totalscore").setValue(this.playerTotalSent);

        getDataFromFirebase();
    }


    private void getDataFromFirebase()
    {
        currentDatabase.child("current_game").child(gameId).addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot)
            {
                if (dataSnapshot.getValue() == null)
                {
                    return;
                }
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren())
                {
                    String bothUsers = dataSnapshot1.getKey();
                    if (!bothUsers.equals(currentUser.getUid()))
                    {
                        otherUserId = bothUsers;
                    }
                }

                if(dataSnapshot.child(currentUser.getUid()).child("toss").exists() && dataSnapshot.child(otherUserId).child("toss").exists())
                {
                    String bol1 = dataSnapshot.child(currentUser.getUid()).child("bool").getValue(String.class);
                    String bol2 = dataSnapshot.child(otherUserId).child("bool").getValue(String.class);
                    if (bol1.equalsIgnoreCase("true") && bol2.equalsIgnoreCase("true"))
                    {
                        String xx = dataSnapshot.child(currentUser.getUid()).child("toss").getValue(String.class);
                        String yy = dataSnapshot.child(otherUserId).child("toss").getValue(String.class);

                        if (xx.equalsIgnoreCase(yy))
                        {
//                            Toast.makeText(OnlineToss.this, "Score was equal", Toast.LENGTH_SHORT).show();
                        }


                        textview1.setText("Opponent : " + yy);
                        textview2.setText("Your : " + xx);

                        playerTotalReceived = dataSnapshot.child(currentUser.getUid()).child("totalscore").getValue(Long.class);
                        opponentTotalReceived = dataSnapshot.child(otherUserId).child("totalscore").getValue(Long.class);

                        textview4.setText("Your Total : " + playerTotalReceived);
                        textview3.setText("Opponent Total : " +opponentTotalReceived);

                        changeImage1(Integer.parseInt(yy));
                        changeImage2(Integer.parseInt(xx));
                        currentDatabase.child("current_game").child(gameId).child(currentUser.getUid()).child("bool").setValue("false");
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError)
            {

            }
        });

    }

    public void calculate(int xx, int yy)
    {

        if(xWas != xx)
        {
            xWas = xx;
            calculate(xWas, yWas);
        }
        if(yWas != yy)
        {
            yWas = yy;
            calculate(xWas, yWas);
        }

        this.playerTotal = this.playerTotal +xx;
        this.otherPlayerTotal = this.otherPlayerTotal +yy;

        textview4.setText("Your Total : " +playerTotal);
        textview3.setText("Opponent Total : " +otherPlayerTotal);

    }

    private void changeImage1(int val)
    {

        if (val == 1)
        {
            imageview1.setImageResource(R.drawable.hands1f);
        }
        if (val == 2)
        {
            imageview1.setImageResource(R.drawable.hands2f);
        }
        if (val == 3)
        {
            imageview1.setImageResource(R.drawable.hands3f);
        }
        if (val == 4)
        {
            imageview1.setImageResource(R.drawable.hands4f);
        }
        if (val == 5)
        {
            imageview1.setImageResource(R.drawable.hands5f);
        }
        if (val == 6)
        {
            imageview1.setImageResource(R.drawable.hand6f);
        }
    }

    private void changeImage2(int val)
    {
        if (val == 1)
        {
            imageview2.setImageResource(R.drawable.hands1f);
        }
        if (val == 2)
        {
            imageview2.setImageResource(R.drawable.hands2f);
        }
        if (val == 3)
        {
            imageview2.setImageResource(R.drawable.hands3f);
        }
        if (val == 4)
        {
            imageview2.setImageResource(R.drawable.hands4f);
        }
        if (val == 5)
        {
            imageview2.setImageResource(R.drawable.hands5f);
        }
        if (val == 6)
        {
            imageview2.setImageResource(R.drawable.hand6f);
        }
    }
    private void  initLogic()
    {
        num = 1;
    }

}